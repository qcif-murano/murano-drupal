#  Copyright 2019 Queensland Cyber Infrastructure Foundation
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

Namespaces:
  =: au.org.nectar.qriscloud
  std: io.murano
  sys: io.murano.system
  conf: io.murano.configuration

Name: Drupal

Extends: std:Application

Properties:
  server:
    Contract: $.class(LAMPBase).notNull()
  mysqlPasswd:
    Contract: $.string().notNull()
  database:
    Contract: $.string().notNull()
  username:
    Contract: $.string().notNull()
  password:
    Contract: $.string().notNull()

Methods:
  initialize:
    Body:
      - $._environment: $.find(std:Environment).require()
      - $._debug: true
      - $._linuxLib: null
      - $._drupal_dir: '/usr/share/drupal7'
      - $._drupal_config: 'sites/default/settings.php'
      - $._drupal_default: 'sites/default/default.settings.php'
      - $._alter_db_clause: null
      - $._db_extra_cfg: ''

  deploy:
    Body:
      - If: not $.getAttr(deployed,false)
        Then:
          - $._environment.reporter.report($this,'Ensuring LAMP is deployed.')
          - $.server.deploy()
          - $._environment.reporter.report($this,'Deploying Drupal')
          - $._linuxLib: new(LinuxUtilLib,$,instance => $.server.instance)
          - $distId: $._linuxLib.distId()
          - $distVersion: $._linuxLib.distVersion()
          - $packageList: 'drupal7 drush'
          - If: $distId = 'ubuntu'
            Then:
              - If: $._debug
                Then:
                  - $._environment.reporter.report($this,'adding package configs')
              - If: $distVersion = 'ubuntu-14.04'
                Then:
                  - $script: |
                      #!/bin/bash
                      sudo debconf-set-selections <<< "postfix postfix/main_mailer_type select Local only"
                      sudo debconf-set-selections <<< "postfix postfix/mailname string localhost"
                      sudo debconf-set-selections <<< "drupal7 drupal7/dbconfig-install boolean false"
                      # ubuntu 14 package not honoring just dbconfig-install
                      sudo debconf-set-selections <<< "dbconfig-common dbconfig-common/dbconfig-install boolean false"
                      sudo debconf-set-selections <<< "drupal7 drupal7/dbconfig-reinstall boolean false"
                      sudo debconf-set-selections <<< "drupal7 drupal7/internal/skip-preseed boolean true"
                Else:
                  - $script: |
                      #!/bin/bash
                      sudo debconf-set-selections <<< "postfix postfix/main_mailer_type select Local only"
                      sudo debconf-set-selections <<< "postfix postfix/mailname string localhost"
                      sudo debconf-set-selections <<< "drupal7 drupal7/dbconfig-install boolean false"
              - conf:Linux.runCommand($.server.instance.agent,$script)
          - $._linuxLib.packageDeploy($packageList)
          - If: $distId = 'ubuntu'
            Then:
              - If: $distVersion = 'ubuntu-14.04'
                Then:
                  - $script: |
                      #!/bin/bash
                      # further ubuntu 14 workarounds
                      cwd=$(pwd)
                      cd /etc/apache2/conf-available
                      sudo ln -sf ../../drupal/7/apache2.conf drupal7.conf
                      sudo sed -i '/  # RewriteBase \/drupal/a \ \ RewriteBase \/drupal7' \
                        /usr/share/drupal7/.htaccess
                      cd $cwd
                  - conf:Linux.runCommand($.server.instance.agent,$script)
              - $script: |
                  #!/bin/bash
                  sudo a2enconf drupal7
                  sudo a2enmod rewrite
              - conf:Linux.runCommand($.server.instance.agent,$script)
          - If: $distId = 'centos'
            Then:
              - $script: |
                  #!/bin/bash
                  sudo sed -i 's/^\([ \t]*Require local\)$/#\1\n    Require all granted/' \
                    /etc/httpd/conf.d/drupal7.conf
                  sudo sed -i '/  # RewriteBase \/drupal/a \ \ RewriteBase \/drupal7' \
                    /usr/share/drupal7/.htaccess
                  sudo sed -i '/\[mysqld\]/a innodb_large_prefix=true\ninnodb_file_format=barracuda\ninnodb_file_per_table=true' \
                    /etc/my.cnf
              - conf:Linux.runCommand($.server.instance.agent,$script)
              - $._linuxLib.serviceCmd('restart','mysql')
              - $._linuxLib.cpFile(format('{dir}/{dft}',dir=>$._drupal_dir,dft=>$._drupal_default),
                  format('{dir}/{cfg}',dir=>$._drupal_dir,cfg=>$._drupal_config))
              - $._alter_db_clause: 'DEFAULT COLLATE utf8mb4_general_ci'
              - $._db_extra_cfg: "\n  'charset' => 'utf8mb4',\n  'collation' => 'utf8mb4_general_ci',"
          - $._linuxLib.serviceCmd('restart','apache')
          - $._environment.reporter.report($this,'Drupal is deployed')
          - $.setAttr(deployed,true)
          - $.configure($.mysqlPasswd,$.database,$.username,$.password)

  configure:
    Arguments:
      - mysqlPasswd:
          Contract: $.string().notNull()
      - database:
          Contract: $.string().notNull()
      - username:
          Contract: $.string().notNull()
      - password:
          Contract: $.string().notNull()
    Body:
      - $.deploy()
      - $._environment.reporter.report($this,'Configuring LAMP')
      - $.server.configure($mysqlPasswd,$database,$username,$password)
      - If: $._alter_db_clause != null
        Then:
          - $.server.alterDatabase($mysqlPasswd,$database,$._alter_db_clause)
      - $._environment.reporter.report($this,'LAMP is configured')
      - $._environment.reporter.report($this,'Configuring Drupal')
      - If: $._debug
        Then:
          - $._environment.reporter.report($this,'updating drupal database settings')
      - $config_drupal: format('{dir}/{cfg}',dir=>$._drupal_dir,cfg=>$._drupal_config)
      - $config: $._linuxLib.getFile($config_drupal)
      - $config_new_template: |
          $databases['default']['default'] = array (
            'database' => '{db}',
            'username' => '{un}',
            'password' => '{pw}',
            'prefix' => '',
            'host' => 'localhost',
            'port' => '3306',
            'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
            'driver' => 'mysql',{cfg}
          );
      - $config_new: format($config_new_template,db=>$database,un=>$username,pw=>$password,cfg=>$._db_extra_cfg)
      - $config: format('{cfg}\n\n{new}',cfg=>$config,new=>$config_new)
      - conf:Linux.putFile($.server.instance.agent,$config,$config_drupal)
      - $script: |
          #!/bin/bash
          cwd=$(pwd)
          cd {dir}
          sudo drush site-install standard --yes --site-name=Drupal --account-name=admin --account-pass='{pw}'
          # enable syslog for fail2ban
          sudo drush en syslog -y
          cd $cwd
      - conf:Linux.runCommand($.server.instance.agent,format($script,dir=>$._drupal_dir,pw=>$password))
      - $._linuxLib.serviceCmd('restart','apache')
      - $._environment.reporter.report($this,'Drupal is configured') 
      - If: $._debug
        Then:
          - $._environment.reporter.report($this,'adding fail2ban config')
      - $fail2ban_name: 'drupal-murano'
      - $fail2ban_filter: |
          [INCLUDES]
          before = common.conf
          
          [Definition]
          failregex = \|user\|<HOST>\|.*\|Login attempt failed (.+)\.$
          ignoreregex =
      - $._linuxLib.putFail2banFilter($fail2ban_name,$fail2ban_filter)
      - $fail2ban_jail: |
          enabled = true
          port = http,https
          filter = {jn}
          banaction = iptables-multiport
      - $._linuxLib.putFail2banJailSyslog($fail2ban_name,format($fail2ban_jail,jn=>$fail2ban_name))
      - $._linuxLib.serviceCmd('restart','fail2ban')
      # add backup config files even if backups are not enabled (in case they need to be enabled later)
      - If: $._debug
        Then:
          - $._environment.reporter.report($this,'adding backup config')
      - $._duplyLib: new(DuplyBackupLib,$,instance => $.server.instance)
      - $stagingDir: $._duplyLib.stagingDir()
      - $exclude_drupal: |
          + {dir}
          + /var/lib/drupal7
      - $._duplyLib.addExcludeConf('40-drupal.conf',format($exclude_drupal,dir=>$._drupal_dir))
      - $proto: 'http'
      - If: $.server.https
        Then:
          - $proto: 'https'
      - $._environment.reporter.report($this,format('Drupal is available at {pr}://{hn}/drupal7',
          pr=>$proto,hn=>$.server.fqdn))
